import React from 'react'
import {Form, Button} from 'semantic-ui-react'

class Flexi extends React.Component{
  state = {}

  handleInputChange = event => this.setState({[event.target.name]: event.target.value})

  getInputField = ({label, name, type}, index) => (
    <Form.Field key={index}>
      <label>{label}</label>
      <input name={name} type={type} onChange={this.handleInputChange}/>
    </Form.Field>
  )

  handleRadioChange = (event, {name, value}) => this.setState({[name]: value})

  getRadioInput = ({label, name, options}, index) => (
    <Form.Group key={index} grouped>
      <label>{label}</label>
      {options.map(option => (
        <Form.Radio
          label={option}
          value={option}
          name={name}
          key={`${index}_${option}`}
          checked={this.state[name] === option}
          onChange={this.handleRadioChange}
        />
      ))}
    </Form.Group>
  ) 

  handleCheckBoxChange = (event, {name, value, checked}) => {
    let list = this.state[name] || []
    list = checked ? [...list, value] : list.filter(v => v !== value)
    this.setState({[name]: list})
  }

  getCheckboxInput = ({label, name, options}, index) => (
    <Form.Group key={index} grouped>
      <label>{label}</label>
      {options.map(option => (
        <Form.Checkbox
          key={`${index}_${option}`}
          label={option}
          value={option}
          name={name}
          checked={(this.state[name] || []).includes[option]}
          onChange={this.handleCheckBoxChange}
        />
      ))}
    </Form.Group>
  )

  handleDropDownChange = ( event, {name, value}) => this.setState({[name] : value})

  getDropDown = ({label, name, values}, index) => (
    <Form.Select
      key={index}
      label={label}
      name={name}
      value={this.state[name]}
      options={values.map(option => ({key: option, text: option, value: option}))}
      onChange={this.handleDropDownChange}
    />
  )

  getFields = (field, index) => {
    const fieldMap = {
      select: this.getDropDown,
      radio: this.getRadioInput,
      checkbox: this.getCheckboxInput
    }
    return fieldMap[field.type] ? fieldMap[field.type](field, index) : this.getInputField(field, index)
  }

  submitForm = (event, data) => {
    event.preventDefault()
    this.props.onSubmit(data)
  }

  getForm = (items = []) => items.length ? (
    <Form onSubmit={(event) => this.submitForm(event, this.state)}>
      {items.map(this.getFields)}
      <Button type='submit'>Submit</Button>
    </Form>
  ) : 'Please add atleast 1 items to config'

  render = () => this.getForm(this.props.config.items)
}

export default Flexi
