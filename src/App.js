import React from 'react';
import {Container, Header, Grid, Icon, Modal, TextArea, Button} from 'semantic-ui-react'
import Flexi from './Flexi'

const flexiConfig = {
  items: [
    {
      "name": "personname",
      "label": "Person's Name",
      "type:": 'text',
    },
    {
      "name": "states",
      "label": "Person's state",
      "type": "select",
      "values": [
        "Maharashtra",
        "Kerala",
        "Tamil Nadu"
      ]
    },
    {
      "name": "gender",
      "label": "Person's Gender",
      "type": "radio",
      "options": [
        "Male",
        "Female",
        "Non-Binary"
      ]
    },
    {
      "name": "identification",
      "label": "Person's Identification",
      "type": "checkbox",
      "options": [
        "DL",
        "RC",
        "Passport",
        "Arms Licence"
      ]
    }
  ]
};

class App extends React.Component {
  state = {
    config: flexiConfig,
    showConfigEditor: false,
    newConfig : '',
    result: ''
  }

  updateConfig = (newConfig) => {
    this.setState({config: JSON.parse(newConfig), showConfigEditor: false})
  }

  getEditConfigModal = (show = false) => (
    <Modal open={show} closeIcon>
      <Modal.Header>Update Config</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <TextArea 
            className="monospace wide" 
            rows={10} 
            autoHeight 
            value={this.state.newConfig}
            onChange={e => this.setState({newConfig: e.target.value})} />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button icon='' content="Load Default config" onClick={() => this.setState({newConfig: JSON.stringify(flexiConfig, null, '\t')})}/>
        <Button icon='check' content='Update Config' onClick={() => this.updateConfig(this.state.newConfig)} />
      </Modal.Actions>
    </Modal>
  )

  onSubmit = (result) => this.setState({result})

  getResultModal = (result = this.state.result) => (
    <Modal 
      trigger={(
        <Button color='red' floated="right" compact >SEE DATA HERE!</Button>)}
      onClose={() => this.setState({result: ''})}>
      <Modal.Content>
        <Modal.Description>
          {Boolean(result) && <pre>{JSON.stringify(result, null, '\t')}</pre>}
        </Modal.Description>
      </Modal.Content>
    </Modal>
  )
  render() {
    return (
      <Container>
        <br/>
        {this.getEditConfigModal(this.state.showConfigEditor)}
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Container>
                <Header onClick = {() => this.setState({showConfigEditor: true})}><Icon name="edit"/> Flexi Config </Header>
                <pre>{JSON.stringify(this.state.config, null, '\t')}</pre>
              </Container>
            </Grid.Column>
            <Grid.Column>
              <Container>
                <Header> Result {this.state.result && this.getResultModal(this.state.result)} </Header>
                <Flexi onSubmit={this.onSubmit} config={this.state.config}/>
              </Container>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default App;
