Write a Flexi React Component that is handed a JSON object that looks
something like:   

`const flexiConfig = {
  items: [
    {
      "name": "personname",
      "label": "Person's Name",
      "type:" TextField,
    },
    {
      "name": "states",
      "label": "Person's state".
      "type": "DropDown",
      "values": [
        "Maharashtra",
        "Kerala",
        "Tamil Nadu"
      ]
    }
  ]
};`  

and is invoked like:
`<Flexi onSubmit={this.onFlexiSubmit} config={flexiConfig}/>`  

In this case, the component creates the appropriate TextField and
DropDown html elements to allow the the user to enter the necessary
information. The component also renders a submit button that calls the
onSubmit function supplied in the props with a Json object as an
argument that contains the user entered values for each of the items in
flexiConfig. For extra credit, allow flexiConfig to be a recursive structure.
